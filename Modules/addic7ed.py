from HTMLParser import HTMLParser
import urllib2
from sub7itles import TvShow
from sub7itles import Subtitle
from sub7itles import Logger

class MyHTMLParser(HTMLParser):

	def __init__(self):
		HTMLParser.__init__(self)
		self.state = 0
		self.ripteam_found = False
		self.extrainfos_found = False
		self.language_found = False
		self.completion_found = False
		self.is_completed = False
		self.ripteam = ''
		self.extrainfos = ''
		self.urldownload = ''
		self.subtitles = []
		
	def get_subtitles(self):
		return self.subtitles
	
	def handle_starttag(self, tag, attrs):
		if tag == "td" :
			dct = dict(attrs)
			if dct.get('class') == "NewsTitle" and dct.get('colspan') != None and dct.get('align') != None :
				self.state = 0
				self.ripteam = ''
				self.extrainfos = ''
				self.urldownload = ''
				self.ripteam_found = True
				self.is_completed = False
			elif dct.get('class') == "newsDate" and dct.get('colspan') != None :
				self.extrainfos_found = True
			elif dct.get('class') == "language" :
				self.language_found = True
			elif self.language_found :
				self.language_found = False
				self.completion_found = True
				
		if tag == "a" :
			dct = dict(attrs)
			if dct.get('class') == "buttonDownload" and dct.get('href') != None and self.is_completed :
				self.urldownload = dct.get('href')
				self.subtitles.append(Subtitle(self.ripteam, self.extrainfos, self.urldownload))
	
	def handle_data(self, data):
		if self.ripteam_found and self.state == 0 :
			Logger().trace('Ripteam : ' + data)
			self.ripteam = data
			self.state = 1
			self.ripteam_found = False
		elif self.extrainfos_found and self.state == 1 :
			Logger().trace('Extrainfos : ' + data)
			self.extrainfos = data
			self.state = 2
			self.extrainfos_found = False
		elif self.completion_found and self.state == 2 :
			Logger().trace('Completion : ' + data)
			self.is_completed = True #Todo test if completed or not	
			self.completion_found = False

def retrieve_subtitles(tvshow):
	page = urllib2.urlopen('http://www.addic7ed.com/serie/' + tvshow.get_titleurlized() + '/' + tvshow.get_season() + '/' + tvshow.get_episode() + '/8')
	html = page.read()
	parser = MyHTMLParser()
	parser.feed(html)
	return parser.get_subtitles()
	
def download_subtitles(tvshow, url):
	req = urllib2.Request('http://www.addic7ed.com' + url)
	req.add_header('Referer', 'http://www.addic7ed.com/serie/' + tvshow.get_titleurlized() + '/' + tvshow.get_season() + '/' + tvshow.get_episode() + '/8')
	page = urllib2.urlopen(req)
	file = open(tvshow.get_filename() + '.srt', 'wb')
	file.write(page.read())
	file.close()
	return True