#include <pybind11/embed.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include "tvshow.h"
#include "subseeker.h"
#include <memory>

PYBIND11_EMBEDDED_MODULE(sub7itles, m) {
	pybind11::class_<TvShow>(m, "TvShow")
		.def(pybind11::init<const std::string&>())
		.def("get_filename", &TvShow::getFilename)
		.def("get_title", &TvShow::getTitle)
		.def("get_titleurlized", &TvShow::getTitleURLized)
		.def("get_season", &TvShow::getSeason)
		.def("get_episode", &TvShow::getEpisode);
	pybind11::class_<Subtitle>(m, "Subtitle")
		.def(pybind11::init<const std::string&, const std::string&, const std::string&>());
	pybind11::class_<spdlog::logger, std::shared_ptr<spdlog::logger>>(m, "Logger")
		.def(pybind11::init([]() {
			return spdlog::get("python");
		}))
		.def("trace", (void (spdlog::logger::*)(const std::string&)) &spdlog::logger::trace);
}

int main(int argc, char* argv[])
{
	auto console = spdlog::stdout_color_mt("console");
	auto python = spdlog::stdout_color_mt("python");
#ifdef _DEBUG
	console->set_level(spdlog::level::trace);
	python->set_level(spdlog::level::trace);
#else
	console->set_level(spdlog::level::err);
	python->set_level(spdlog::level::err);
#endif

	if (argc > 1)
	{
		console->trace(argv[1]);

		pybind11::scoped_interpreter python{};

		TvShow searchShow(argv[1]);
		SubSeeker subSeeker("./Modules/");
		std::vector<Subtitle> subtitles = subSeeker.retrieveSubtitles(searchShow);
		searchShow.addSubtitles(subtitles);
		if (subSeeker.downloadSubtitle(searchShow))
		{
			console->info("Subtitle downloaded !");
		}
	}
	else
	{
		console->error("No filename");
	}
	return 0;
}