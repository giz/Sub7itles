#include "subseeker.h"
#include "tvshow.h"
#include "subtitle.h"
#include <filesystem>
#include <pybind11/stl.h>
#include <spdlog/spdlog.h>

SubSeeker::SubSeeker(std::string pythonmodulespath)
{
	// Import python modules
	pybind11::module::import("sys").attr("path").cast<pybind11::list>().append(pythonmodulespath);	// Include pythonmodulespath in python sys.path
	uint8_t moduleid = 0;
	for (auto& p : std::filesystem::directory_iterator(pythonmodulespath))
	{
		if (p.path().extension() == std::filesystem::path(".py"))
		{
			spdlog::get("console")->trace("Module name found : {}", p.path().stem().string());
			m_PythonModules.insert(std::make_pair(moduleid, pybind11::module::import(p.path().stem().string().c_str())));
			++moduleid;
		}
	}

	spdlog::get("console")->trace("Modules loaded : {}", m_PythonModules.size());
}

std::vector<Subtitle> SubSeeker::retrieveSubtitles(const TvShow& tvshow)
{
	std::vector<Subtitle> subtitles;
	for (std::unordered_map<uint8_t, pybind11::module>::iterator itmodule = m_PythonModules.begin(); itmodule != m_PythonModules.end(); ++itmodule)
	{
		try
		{
			std::vector<Subtitle> subs = (*itmodule).second.attr("retrieve_subtitles")(tvshow).cast<std::vector<Subtitle>>();	// Call python module function retrieve_subtitles()
			for (std::vector<Subtitle>::iterator itsub = subs.begin(); itsub != subs.end(); ++itsub)
			{
				(*itsub).setPythonModuleID((*itmodule).first);
			}
			subtitles.insert(subtitles.end(), subs.begin(), subs.end());	// Append retrieved subtitles to subtitles
		}
		catch (pybind11::error_already_set const &pythonErr)
		{
			spdlog::get("python")->error(pythonErr.what());
		}
	}

	return subtitles;
}

bool SubSeeker::downloadSubtitle(const TvShow& tvshow, int index /*= -1*/)
{
	if (index < 0)
	{
		// Download best subtitle
		if (tvshow.getSubtitles().size() > 0)
		{
			/** \todo Improve best subtitle searching */
			const std::vector<Subtitle>& subtitles = tvshow.getSubtitles();
			for (std::vector<Subtitle>::const_reverse_iterator it = subtitles.rbegin(); it != subtitles.rend(); ++it)
			{
				if ((*it).match(tvshow))
				{
					spdlog::get("console")->trace("Match : {}", (*it).getDownloadURL());

					try
					{
						return m_PythonModules[(*it).getPythonModuleID()].attr("download_subtitles")(tvshow, (*it).getDownloadURL()).cast<bool>();	// Call python module function download_subtitles()
					}
					catch (pybind11::error_already_set const &pythonErr)
					{
						spdlog::get("python")->error(pythonErr.what());
						return false;
					}
				}
			}

			return false;	// No match
		}
	}
	else
	{
		// Download subtitle at index
		if (tvshow.getSubtitles().size() > static_cast<size_t>(index))
		{
			const Subtitle& subtitle = tvshow.getSubtitles().at(index);
			return m_PythonModules[subtitle.getPythonModuleID()].attr("download_subtitles")(tvshow, subtitle.getDownloadURL()).cast<bool>();	// Call python module function download_subtitles()
		}
	}

	return false;
}
