#include "tvshow.h"
#include <regex>
#include <algorithm>
#include <spdlog/spdlog.h>
#include <pybind11/stl.h>

TvShow::TvShow(std::string filename) :
	m_Title(""),
	m_Season(""),
	m_Episode(""),
	m_RipTeam(""),
	m_Codec(""),
	m_Quality(""),
	m_Resolution("")
{
	filename.erase(filename.find_last_of('.'), 4);	// Remove extension
	m_Filename = filename;
	std::replace(filename.begin(), filename.end(), '.', ' ');

	spdlog::get("console")->trace("Filename : {}", filename);

	std::smatch match;
	// Try to extract title, season and episode from filename
	if (std::regex_search(filename, match, std::regex("([a-zA-Z0-9 '()]+)s([0-9]{1,2})e([0-9]{1,2})|([a-zA-Z0-9 '()]+)([0-9]{1,2})x([0-9]{1,2})", std::regex_constants::icase)) && match.size() > 4)
	{
		m_Title = match.str(1);
		m_Title = m_Title.erase(m_Title.find_last_not_of("\t\n\v\f\r ") + 1).erase(0, m_Title.find_first_not_of("\t\n\v\f\r "));	// Trim
		m_Season = match.str(2);
		m_Season.erase(0, std::min<size_t>(m_Season.find_first_not_of('0'), m_Season.size() - 1));
		m_Episode = match.str(3);
		m_Episode.erase(0, std::min<size_t>(m_Episode.find_first_not_of('0'), m_Episode.size() - 1));

		spdlog::get("console")->trace("Title : {}", m_Title);
		spdlog::get("console")->trace("Season : {}", m_Season);
		spdlog::get("console")->trace("Episode : {}", m_Episode);
	}

	// Try to extract quality from filename
	if (std::regex_search(filename, match, std::regex("(BDRip|HDRip|DVDRip|WEBRip|BluRay|HDTV|WEBDL|WEB-DL|WEB DL)", std::regex_constants::icase)) && match.size() > 1)
	{
		m_Quality = match.str(1);

		spdlog::get("console")->trace("Quality : {}", m_Quality);
	}

	// Try tro extract rip team from filename
	if (std::regex_search(filename, match, std::regex("([a-zA-Z0-9]+)[a-zA-Z0-9\\[\\]]*$", std::regex_constants::icase)) && match.size() > 1)
	{
		m_RipTeam = match.str(1);

		spdlog::get("console")->trace("Rip Team : {}", m_RipTeam);
	}

	// Add alternative rip teams
	if (m_RipTeam != "")
	{
		try
		{
			std::list<std::list<std::string>> alts = pybind11::module::import("config").attr("altripteams").cast<std::list<std::list<std::string>>>();	// Load the list from config.py
			for (std::list<std::list<std::string>>::iterator it = alts.begin(); it != alts.end(); ++it)
			{
				spdlog::get("console")->trace("Alts");
				if (std::find((*it).begin(), (*it).end(), m_RipTeam) != (*it).end())
				{
					m_AltRipTeams.insert(m_AltRipTeams.end(), (*it).begin(), (*it).end());	// Append the list
				}
			}

			for (std::list<std::string>::iterator it = m_AltRipTeams.begin(); it != m_AltRipTeams.end(); ++it)
			{
				spdlog::get("console")->trace("Alternative rip team : {}", (*it));
			}
			
		}
		catch (pybind11::error_already_set const &pythonErr)
		{
			spdlog::get("python")->error(pythonErr.what());
		}
	}
}

const std::string& TvShow::getFilename() const
{
	return m_Filename;
}

const std::string& TvShow::getTitle() const
{
	return m_Title;
}

const std::string TvShow::getTitleURLized() const
{
	std::string titleurlized = m_Title;
	std::replace(titleurlized.begin(), titleurlized.end(), ' ', '+');
	return titleurlized;
}

const std::string& TvShow::getSeason() const
{
	return m_Season;
}

const std::string& TvShow::getEpisode() const
{
	return m_Episode;
}

const std::string& TvShow::getRipTeam() const
{
	return m_RipTeam;
}

const std::string& TvShow::getCodec() const
{
	return m_Codec;
}

const std::string& TvShow::getQuality() const
{
	return m_Quality;
}

const std::string& TvShow::getResolution() const
{
	return m_Resolution;
}

const std::list<std::string>& TvShow::getAlternativeRipTeams() const
{
	return m_AltRipTeams;
}

const std::vector<Subtitle>& TvShow::getSubtitles() const
{
	return m_Subtitles;
}

void TvShow::addSubtitles(std::vector<Subtitle> subtitles)
{
	m_Subtitles.insert(m_Subtitles.end(), subtitles.begin(), subtitles.end());	// Append subtitles to m_Subtitles
}
